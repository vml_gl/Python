import math
def combination (n, k):
  return(int(math.factorial(n) / (math.factorial(k) * math.factorial(n-k))))

def pascal_triangle(rows):
  result = []
  for i in range(rows):
    row = []
    for j in range(i+1):
      row.append(combination(i, j))
    result.append(row)
  return result
  
  
for r in pascal_triangle(7):
  print(" ".join(map(str, r)))